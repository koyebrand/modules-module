<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// for Backend:

/*
 * Backend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    /*
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the backend,
     * then limit the backend features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     * These routes can not be hit if the password is expired
     */
    //include_route_files(__DIR__.'/backend/');
    Route::group([
        'prefix'     => 'modules',
        'as'         => 'modules.',
        //'namespace'  => 'Crud',
    ], function () {
        Route::group([
            'middleware' => 'permission:edit data',
        ], function () {
            Route::get('', 'ModulesController@index')->name('index');
            Route::get('enable/{module}',  'ModulesController@enableModule' )->name('enablemodule');
            Route::get('disable/{module}', 'ModulesController@disableModule')->name('disablemodule');
            //Route::get('indexbyorg/{organisation_id?}', 'UserController@index')->name('indexbyorg');
            //Route::get('datatabledata', 'UserController@datatabledata')->name('datatabledata');
            //Route::any('export/{filter?}', 'UserController@export')->name('export');
            //Route::get('edit/{id}', 'UserController@edit')->name('edit');
            //Route::get('create/{organisation_id?}', 'UserController@create')->name('create');
            //Route::any('update/{id?}', 'UserController@update')->name('update');
            //Route::any('delete/{project}', 'UserController@destroy')->name('delete');
        });
    });
});

// for frontend:

/*
Route::prefix('modules')->group(function() {
    Route::get('/', 'ModulesController@index');
});
*/