<?php

namespace Modules\Modules\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Module;

class ModulesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        
        //dd(Module::toCollection());
        //dd(Module::allEnabled());
        $modules = [
            'enabled'   => Module::allEnabled(),
            'disabled'   => Module::allDisabled(),
        ];
        //$mods = Module::toCollection()->toArray();
        //dd($mods);
        return view('modules::backend.index',compact('modules'));
    }

    public function enableModule($moduleName)
    {
        $module = Module::find($moduleName);
        $module->enable();
        return redirect()->back()->withFlashSuccess(__('modules::general.messages.module_enabled'));
    }
    public function disableModule($moduleName)
    {
        if($moduleName=='settings'){
            return redirect()->back()->withFlashDanger(__('modules::general.messages.module_not_disabled'));
        }
        $module = Module::find($moduleName);
        $module->disable();
        return redirect()->back()->withFlashSuccess(__('modules::general.messages.module_disabled'));
    }
}
