<?php

return [
    'name' => 'Modules',

    'iconclass' => 'fas fa-cubes',
    'section' => 'admin',
    'permission' => 'module-admin',
    'cannotbedisabled' => true,
];
