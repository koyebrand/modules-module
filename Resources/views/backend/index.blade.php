@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('modules::general.name'))

@section('breadcrumb-links')
    
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    <i class="fas fa-cubes text-primary"></i> {{ __('modules::general.name') }}
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">@lang('modules::general.labels.active_modules')</h5>
                       
                        <div class="table-responsive">
                            <table class="table table-bordered_" id="users-table">

                                <tbody>
                                    @foreach ($modules['enabled'] as $module)
                                    <tr>
                                        <td><i class="{{Config::get($module->getLowerName().'.iconclass','fas fa-cube') }} text-primary"></i> {{ $module->getName() }}</td>
                                        <td class="text-right">
                                            @if($module->getLowerName()!='settings' && !Config::get($module->getLowerName().'.cannotbedisabled',false) )
                                            <a class="btn btn-sm btn-danger" href="/admin/modules/disable/{{ $module->getLowerName()}}">@lang('modules::general.labels.disable')</a>
                                            @else
                                            <button class="btn btn-sm btn-danger" disabled="disabled">@lang('modules::general.labels.disable')</button>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div><!--col-->

            <div class="col">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{ __('modules::general.labels.inactive_modules') }}</h5>
                       
                        <div class="table-responsive">
                            <table class="table table-bordered_" id="users-table">

                                <tbody>
                                    @foreach ($modules['disabled'] as $module)
                                    <tr>
                                        <td><i class="{{Config::get($module->getLowerName().'.iconclass','fas fa-cube') }}  text-primary"></i> {{ $module->getName() }}</td>
                                        <td class="text-right"><a class="btn btn-sm btn-success" href="/admin/modules/enable/{{ $module->getLowerName()}}">@lang('modules::general.labels.enable')</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


                
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
<script>
$(function() {
    
});
</script>
@endpush