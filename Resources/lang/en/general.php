<?php

return [
    'name' => 'Modules',
    'edit_title' => 'Manage Modules',

    'labels'=>[
        'active_modules' => 'Active Modules',
        'inactive_modules' => 'Inactive Modules',
        'enable' => 'Enable',
        'disable' => 'Disable',
    ],
    'messages' => [
        'module_enabled'=>'The Module was activated.',
        'module_disabled'=>'The Module was disabled.',
        'module_not_disabled' => 'This Module cannot be disabled.'
    ]
];