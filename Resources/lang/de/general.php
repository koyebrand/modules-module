<?php

return [
    'name' => 'Module',
    'edit_title' => 'Module bearbeiten',

    'labels'=>[
        'active_modules' => 'Aktivierte Module',
        'inactive_modules' => 'Inaktive Modules',
        'enable' => 'Aktivieren',
        'disable' => 'Deaktivieren',
    ],

    'messages' => [
        'module_enabled'=>'Das Modul wurde aktiviert.',
        'module_disabled'=>'Das Modul wurde deaktiviert.',
        'module_not_disabled' => 'Dieses Modul kann nicht deaktiviert werden.'
    ]
];